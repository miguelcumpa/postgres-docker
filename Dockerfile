FROM postgres:12.3

RUN apt update && apt install -y postgis postgresql-12-postgis-3 postgresql-12-ogr-fdw

COPY ./init-server.sh /docker-entrypoint-initdb.d/init-server.sh
